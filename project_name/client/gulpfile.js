var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano'),
    package = require('./package.json'),
    babel = require('gulp-babel'),
    browserify = require('gulp-browserify'),
    livereload = require('gulp-livereload');

gulp.task('css', function () {
    return gulp.src('scss/main.scss')
    .pipe(sass())
    .pipe(autoprefixer('last 4 version'))
    .pipe(gulp.dest('static/css'))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('static/css'))
	.pipe(livereload());
});

gulp.task('js',function(){
  gulp.src('js/main.js')
    .pipe(browserify())
    .pipe(babel({
        presets: ['es2015']
    }))
    //.pipe(minify())
    .pipe(gulp.dest('static/js'))
    //.pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('static/js'))
	.pipe(livereload());
});


gulp.task('default', ['css', 'js'], function () {
	livereload.listen();
    gulp.watch("scss/**/*", ['css']);
    gulp.watch("js/**/*", ['js']);
});
