# Cms and account #

### Project includes: ###

- Django 1.8.*
- Django cms
- User account
- Multilanguage

----------


### Usage: ###

    django-admin.py startproject --template=https://bitbucket.org/bezobzira/django-project18/get/master.zip <project_name>

----------

### Installation: ###

    pip install virtualenv
    virtualenv mysite-env
    source mysite-env/bin/activate
    django-admin.py startproject --template=https://bitbucket.org/bezobzira/django-project18/get/master.zip <project_name>
    cd mysite
    pip install -r requirements/requirements.txt / requirements/windows/requirements.txt
	manage.py makemigrations (after adding languages / needed for modetranslation)
    manage.py migrate
    manage.py runserver

----------

### Extra files ###


#### Settings ####

- settings.py (optimized)
- local_settings.py (needs optimization for email)
- urls.py (optimized)


#### _local_settings.py_: ####

Here you can add:

- Database (name of DB equals to project name)
- Email data

**Settings.py must remain untouched**

----------
#### Templates ####

- `base.html`
- `_account_bar.html`
- `_footer.html`
- Admin
	- Custom admin templates (fwd support header)
- Menu
	- `breadcrumb.html`
	- `dummy.html`
	- `empty.html`
	- `language_chooser.html`
	- `menu.html`
	- `menu_bootstrap.html`
	- `sub_menu.html`
- Layouts
	- `content_with_slider.html`
	- `content_without_slider.html`
	- `home.html`
	- `location.html`
	- extensions
		- `slider.html`


----------


### Requirements included: ###

Django==1.8.*
django-appconf
django-bootstrap-form
django-classy-tags
django-cms
django-jsonfield
django-modeltranslation
django-mptt
django-sekizai
django-treebeard
django-user-accounts
djangocms-admin-style
djangocms-text-ckeditor
easy-thumbnails
eventlog
html5lib
metron
MySQL-python
Pillow
pinax-theme-bootstrap
pytz
six
yolk





